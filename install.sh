#!/bin/bash
echo "installing some packages..."
sudo apt install pciutils-dev libpci-dev -y
INSTALLER_PATH=$(pwd)
VERSION_FILE=miner_version.txt

function InstallUpdator {
    echo "install_updator_script_started"
    if [ -f /root/data/$VERSION_FILE ] ; then
        echo "Updator already installed!"
    else
        echo "installing updator"
        cp $INSTALLER_PATH/version.txt /root/data/$VERSION_FILE
        mkdir /root/data/updator
        cd /root/data/updator
        echo '
#!/bin/bash
while true
do
	echo "Checking for updates"
    cd /root/data/updator
    wget -O v.txt https://gitlab.com/Dave2588/miner_setup_script/-/raw/master/version.txt
    NEW_VERSION=$(cat v.txt)
    CURRENT_VERSION=$(cat /root/data/miner_version.txt)
    if [[ $NEW_VERSION > $CURRENT_VERSION ]]
    then
        echo "Updating miner"
        rm -r /root/data/miner_setup_script
        cd /root/data/
        git clone https://gitlab.com/Dave2588/miner_setup_script.git
        screen -X -S TeamRedMiner quit
        cd miner_setup_script
        ./install.sh
        cp /root/data/updator/v.txt /root/data/miner_version.txt
    fi
	sleep 300
done'>updator.sh
        chmod +x updator.sh
        screen -dmS AutomaticUpdate bash ./updator.sh
    fi

}

MINER_SCRIPT_NAME=kopac.sh
CPU_MINER_SCRIPT_NAME=cpu_miner.sh
if test -f teamredminer; then
    echo "teamredminer exists."
    screen -X -S XMRig quit
    #SETUP MINER
    rm -r /root/data/miner
    cd /root/data
    mkdir miner
    cd miner
    echo "getting cube name"
    CUBE_NAME=$(cat /root/info/cube_name.txt)
    #CUBE_NAME="update_test"
    echo "#!/bin/sh">$MINER_SCRIPT_NAME
    echo "export GPU_MAX_ALLOC_PERCENT=100">>$MINER_SCRIPT_NAME
    echo "export GPU_SINGLE_ALLOC_PERCENT=100">>$MINER_SCRIPT_NAME
    echo "export GPU_MAX_HEAP_SIZE=100">>$MINER_SCRIPT_NAME
    echo "export GPU_USE_SYNC_OBJECTS=1">>$MINER_SCRIPT_NAME
    echo "while true">>$MINER_SCRIPT_NAME
    echo "do">>$MINER_SCRIPT_NAME
    echo "./teamredminer -a ethash -o stratum+tcp://eu.ezil.me:5555 -u 0xc5ff78543903ebc2ee9736ef4d2d76d29b6796e5.zil1henvf0t30pmgayf2tcla49s7g7nr26kj06qr7v --eth_worker $CUBE_NAME">>$MINER_SCRIPT_NAME
    echo "sleep 1">>$MINER_SCRIPT_NAME
    echo "done">>$MINER_SCRIPT_NAME
    chmod +x $MINER_SCRIPT_NAME
    #XMRig CPU mining
    echo "#!/bin/sh">$CPU_MINER_SCRIPT_NAME
    echo "while true">>$CPU_MINER_SCRIPT_NAME
    echo "do">>$CPU_MINER_SCRIPT_NAME
    echo "./xmrig -o xmr.pool.gntl.co.uk:10009 -a rx/0 -u 83FcQWgkDfQZFnPcyXjXNbUrdR8hreJsnKkV52dz6Tq6Erw5bjexaHR8EGSqP4cySmgkwKmQmNqwJ27qTHWdXjAAV9SmbNQ -p $CUBE_NAME -k -t 1 --tls
">>$CPU_MINER_SCRIPT_NAME
    echo "sleep 1">>$CPU_MINER_SCRIPT_NAME
    echo "done">>$CPU_MINER_SCRIPT_NAME
    chmod +x $CPU_MINER_SCRIPT_NAME
    #XMRig CPU mining
    mv $INSTALLER_PATH/teamredminer /root/data/miner/teamredminer
    chmod +x /root/data/miner/teamredminer
    mv $INSTALLER_PATH/xmrig /root/data/miner/xmrig
    chmod +x /root/data/miner/xmrig
    echo '
#!/bin/bash
cd /root/data
screen -dm -S script bash /root/data/script.bash
echo root:`cat /root/info/cube_password.txt` | chpasswd
echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
service ssh restart
rm -R /etc/update-motd.d/*
echo "Welcome to Cubelogy" > /etc/motd
cd /root/data/updator
screen -dmS AutomaticUpdate bash ./updator.sh
/root/data/miner/
screen -dmS TeamRedMiner /root/data/miner/./'$MINER_SCRIPT_NAME> /root/data/startup.bash
    echo "screen -dmS XMRig /root/data/miner/./$CPU_MINER_SCRIPT_NAME"> /root/data/startup.bash
    chmod +x /root/data/startup.bash
    screen -X -S TeamRedMiner quit
    sleep 2
    screen -dmS TeamRedMiner /root/data/miner/./$MINER_SCRIPT_NAME
    screen -dmS XMRig /root/data/miner/./$CPU_MINER_SCRIPT_NAME
    #SETUP UPDATOR
    InstallUpdator
    exit
else
    echo "Cant install, probbably already installed or teamredminer is missing"
    exit

fi
